const path = require ('path')
const app = require('../app')

/* LOG IN PAGE*/
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'Views', 'login.html'))
}

/* SIGN UP PAGE*/
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'Views', 'signup.html'))
}

/* HOME PAGE*/
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'Views', 'index.html'))
}
