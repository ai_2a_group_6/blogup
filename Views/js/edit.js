function handlePictureUpload(event) {
    const reader = new FileReader();
    reader.onload = function() {
        const image = document.getElementById("profile-picture");
        image.src = reader.result;
    }
    reader.readAsDataURL(event.target.files[0]);
}
// Open the password change pop-up
function openPasswordChangePopup() {
    document.getElementById("passwordChangePopup").style.display = "block";
  }

// Close the password change pop-up
function closePasswordChangePopup() {
  document.getElementById("passwordChangePopup").style.display = "none";
}

// Handle saving password changes
function savePasswordChanges() {
  // Retrieve the current password, new password, and confirm new password values
  var currentPassword = document.getElementById("currentPassword").value;
  var newPassword = document.getElementById("newPassword").value;
  var confirmNewPassword = document.getElementById("confirmNewPassword").value;

  // Perform password validation and save changes if valid
  if (newPassword.length >= 8 && newPassword === confirmNewPassword) {
    // TODO: Perform password change logic here

    // Close the password change pop-up
    closePasswordChangePopup();
  } else {
    // Display an error message or handle validation errors
    alert("Invalid password or passwords do not match.");
  }
}
