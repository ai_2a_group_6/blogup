const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, 'Please enter your username'],
    },
    email: {
        type: String,
        required: [true, 'Please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, "Please provide your valid email"],
    },
    phonenumber: {
        type: Number,
        validate: {
            validator: function(value) {
                // Convert the input value to a string and then use regular expression to match against the required pattern
                // The pattern checks for a string that starts with either '17' or '77' and is followed by 6 digits
                return /^(17|77)\d{6}$/.test(value.toString());
            },
            message: 'Please provide a valid phone number in the format 17xxxxxx or 77xxxxxx'
        },
        required: [true, 'Please provide a phone number!']
    },
    password: {
        type: String,
        required: [true, "Please provide a password"],
        minlength: 6,
        select: false,
    },
    passwordConfirm: {
        type: String,
        required: [true, 'Please confirm your password'],
        validate: {
            validator: function (el) {
                return el === this.password
            },
            message: 'Passwords are not the same',
        },
    },
    active: {
        type: Boolean,
        default:true,
        select: false,
    },

})

userSchema.pre('save',async function  (next) {
    if(!this.isModified('password')) return next()
    this.password = await bcrypt.hash(this.password, 12)
    this.passwordConfirm = undefined
    next()
})

userSchema.pre('findOneAndUpdate', async function (next) {
    const update = this.getUpdate();
    if (update.password !== '' &&
    update.password !== undefined &&
    update.password == update.passwordConfirm) {

        this.getUpdate().password = await bcrypt.hash(update.password, 12)

        update.passwordConfirm = undefined
        next()
    }else
    next()
})

userSchema.methods.correctPassword = async function (
    candidatePassword,
    userPassword
) {
    return await bcrypt.compare(candidatePassword, userPassword)
}

const User = mongoose.model('User', userSchema)
module.exports = User