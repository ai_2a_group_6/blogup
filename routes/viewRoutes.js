const express = require ('express')
const router = express.Router()
const viewController = require('./../controllers/viewControllers')


router.get('/', viewController.getHome)
router.get('/login', viewController.getLoginForm)
router.get('/signup', viewController.getSignupForm)

module.exports = router